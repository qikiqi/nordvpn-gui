#!/usr/bin/python3.4
# -*- coding: utf-8 -*-
## BUGAREA:
#
## When pressing refresh, this might happen
# Traceback (most recent call last):
#   File "./pingnordvpn_mainv2.py", line 110, in refreshInfo
#     self.nord.refreshInfo()
#   File "./pingnordvpn_mainv2.py", line 301, in refreshInfo
#     self.town, self.country = self.getCity(self.lat1, self.lon1)
#   File "./pingnordvpn_mainv2.py", line 321, in getCity
#     components = j['results'][0]['address_components']
# IndexError: list index out of range
# Traceback (most recent call last):
#   File "./pingnordvpn_mainv2.py", line 110, in refreshInfo
#     self.nord.refreshInfo()
#   File "./pingnordvpn_mainv2.py", line 301, in refreshInfo
#     self.town, self.country = self.getCity(self.lat1, self.lon1)
#   File "./pingnordvpn_mainv2.py", line 321, in getCity
#     components = j['results'][0]['address_components']
# IndexError: list index out of range
# Traceback (most recent call last):
#   File "./pingnordvpn_mainv2.py", line 110, in refreshInfo
#     self.nord.refreshInfo()
#   File "./pingnordvpn_mainv2.py", line 301, in refreshInfo
#     self.town, self.country = self.getCity(self.lat1, self.lon1)
#   File "./pingnordvpn_mainv2.py", line 321, in getCity
#     components = j['results'][0]['address_components']
# IndexError: list index out of range

import sys
import codecs
import re
import urllib
import json
import pprint as pp
import subprocess
from math import radians, cos, sin, asin, sqrt
import requests
import random
import time
import numpy as np
import os

from PyQt5 import QtCore, QtGui, QtWidgets

from pingnordvpn import Ui_MainWindow
from settingsWindow3 import Ui_Dialog
from threadClass import ThreadClass, CommunicatorClass, RefreshThreadClass, ConnectionTestThreadClass, SpeedtestThreadClass, ConfiguratorThreadClass

### TODO:
#   sudoers:
#        user ALL=(ALL) NOPASSWD: /home/user/nordvpn-gui/kills.sh
#        user ALL=(ALL) NOPASSWD: /usr/sbin/openvpn
#   Create shortcut for creation of .desktop file with correct settings
#   Add a check that makes sure that CONFIG files exists. POPUP DIALOG if not found
#   Create a proper documentation that follows doxygen formatting
#   Add total download / upload since vpn connection begun (IF POSSIBLE)
#   Create own windows for speedtest + STATUSBAR QUICKSPEEDTEST
    # Log for all the speedtests that have been done (+ ip, time, typeofconnection)
#   Make sure that "scrolldown" works
#   Move setting tables to certain length to work automatically after update via signal
#   Create option to disable automatic iptesting because servers the pinged servers have limits (AVOID BY USING NORDVPN IP TEST)
#   Make settings remember GEOMETRY (custom closeEvent), APPLY AUTOMATICALLY FILTERS, ORDERING OF ROWS
#   Create 'favorites' and make it so that it tests speeds (option) and connects where is lowest load (and highest speed)
# DISABLE CONSTANT IP CHECKING AND INSTEAD PING NORDVPN PROTECTED / UNPROTECTED AND IF STATUS CHANGES THEN!! CHECK THE IP SITUATION

class PingNordVPN(Ui_MainWindow):
    """class for main window"""

    def __init__(self, MainWindow):
        ## Setup the mainwindow
        super(PingNordVPN, self).__init__()
        ## OLD
        # Ui_MainWindow.__init__(self)
        # self.setupUi(MainWindow)
        ## OLD
        self.ui = Ui_MainWindow()
        self.ui.setupUi(MainWindow)
        MainWindow.setWindowTitle("norvpn-gui 0.1a")
        ## Get the homefolder and current path
        self.homefolder = os.getenv("HOME") + "/"
        self.guifolder = os.getcwd() + "/"


        ## First setup the "physical" elements
        # progressBar
        self.ui.progressBar.reset()
        self.ui.progressBar.setMinimum(0)
        self.ui.progressBar.setMaximum(100)
        self.ui.progressBar.setTextVisible(True)
        self.testProgress(0)

        # Cosmetic changes
        # tableServer
        self.ui.tableServers.setHorizontalHeaderLabels(('Server', 'Load', 'Distance', 'Mode'))
        self.ui.tableServers.horizontalHeader().setSectionResizeMode(0, 2)
        self.ui.tableServers.horizontalHeader().setSectionResizeMode(1, 2)
        self.ui.tableServers.horizontalHeader().setSectionResizeMode(2, 2)
        self.ui.tableServers.horizontalHeader().setSectionResizeMode(3, 1)
        self.ui.tableServers.verticalHeader().setSectionResizeMode(2)
        self.ui.tableServers.horizontalHeader().resizeSection(0, 175)
        self.ui.tableServers.horizontalHeader().resizeSection(1, 65)
        self.ui.tableServers.horizontalHeader().resizeSection(2, 65)
        #self.tableServers.horizontalHeader().resizeSection(3, 350)
        self.ui.tableServers.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)

        # tableInfo
        self.ui.tableInfo.setHorizontalHeaderLabels(('IP Address', 'Country', 'City / Town', 'Last refresh', ''))
        self.ui.tableInfo.horizontalHeader().setSectionResizeMode(0, 2)
        self.ui.tableInfo.horizontalHeader().resizeSection(0, 170)
        self.ui.tableInfo.horizontalHeader().setSectionResizeMode(1, 2)
        self.ui.tableInfo.horizontalHeader().resizeSection(1, 160)
        self.ui.tableInfo.horizontalHeader().setSectionResizeMode(2, 1)
        self.ui.tableInfo.horizontalHeader().resizeSection(2, 280)
        self.ui.tableInfo.horizontalHeader().setSectionResizeMode(3, 1)
        self.ui.tableInfo.horizontalHeader().resizeSection(2, 250)
        self.ui.tableInfo.horizontalHeader().setSectionResizeMode(4, 2)
        self.ui.tableInfo.horizontalHeader().resizeSection(4, 70)
        self.ui.tableInfo.verticalHeader().setSectionResizeMode(2)
        self.ui.tableInfo.verticalHeader().setVisible(0)
        self.ui.tableInfo.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)

        # rest dont?
        self.ui.tableInfo.setRowCount(1)

        # tableInfo refreshbutton
        self.ui.refreshInfobtn = QtWidgets.QPushButton()
        self.ui.refreshInfobtn.clicked.connect(self.altRefreshInfo)
        self.ui.refreshInfobtn.setText("Refresh")
        self.ui.tableInfo.setCellWidget(0, 4, self.ui.refreshInfobtn)

        ## Then setup all threads etc. which might end up modifying GUI
        # Create the Refresher
        self.updater = RefreshThreadClass()
        self.updater.sigref.connect(self.nordhandler)

        # Test for connection
        self.activeConnection = False
        self.offlineDetect(self.activeConnection)
        self.connectionTester = ConnectionTestThreadClass()
        self.connectionTester.offline.connect(self.offlineDetect)
        #self.connectionTester.start()

        # Initialize dialog
        self.dialog = QtWidgets.QDialog()
        self.dialog.ui = Ui_Dialog()
        self.dialog.ui.setupUi(self.dialog)
        self.dialog.ui.lineEditPassword.setEchoMode(2)

        ## Initialize settings
        QtCore.QCoreApplication.setOrganizationName("qqq")
        QtCore.QCoreApplication.setApplicationName("NordVPNGUI")
        self.settings = QtCore.QSettings()
        self.setSettings2dialog()

        # Initialize communicator to send messages
        self.communicator = CommunicatorClass(self)

        # Initialize threadClass which handles updating the servers
        self.tester = ThreadClass(self, self.communicator)
        self.tester.getData.connect(self.addTableServers)
        self.tester.getInfoData.connect(self.addTableInfo)
        self.tester.start()
        self.tester.wait()

        # Initialize UNSECURED & SECURED tests
        self.connectionSecured = False

        # Initialize SpeedtestThreadClass which tests connection speed
        self.speedtester = SpeedtestThreadClass()
        self.speedtester.result.connect(self.showSpeedtestResult)

        # Initialize ConfiguratorThreadClass which handles downloading and modifying confs
        self.confthread = ConfiguratorThreadClass(mode=0)

        # These depend on threads / others
        self.ui.tableServers.setRowCount(len(self.tester.nord.dataServer))
        self.dialogCheckstates, self.dialogLineEdits = self.getDialogCheckboxStates(), self.getDialogLineEdits()

        # PROCESS WORKs
        # QProcess object for external app
        self.process = QtCore.QProcess(MainWindow)
        # QProcess emits `readyRead` when there is data to be read
        self.process.readyRead.connect(self.dataReady)

        # TIMER WORKs
        multitimer = QtCore.QTimer(MainWindow)
        multitimer.timeout.connect(self.multishot)
        multitimer.start(5000)

        ## After create the connections for GUI
        # Connect buttons
        self.ui.buttonConnect.clicked.connect(self.startSpeedtest)
        self.ui.buttonAutoconnect.clicked.connect(self.connect_vpn)

        # Connect dialog buttons
        self.dialog.ui.buttonBox.accepted.connect(self.setDialog2Main)
        self.dialog.ui.buttonAdd.clicked.connect(self.add2Kill)
        self.dialog.ui.buttonRemoveSelected.clicked.connect(self.removeSelectedKill)
        self.dialog.ui.buttonDownloadConf.clicked.connect(lambda: self.configuratorHandler(0))
        self.dialog.ui.checkAuth.stateChanged.connect(lambda state: self.configuratorHandler(1, state))

        # Connect actions
        self.ui.actionQuit.triggered.connect(self.closeEvent)
        self.ui.actionSettings.triggered.connect(self.windowSettings)

        # Apply settings from dialog to mainwindow
        self.setDialog2Main()

        # TODO: This logic was done in very much in a hurry so it
        #       should be revisited and possibly revised
        # Connection notifications
        self.numConnected = 0
        self.numDisconnected = 0
        self.numConnectedReset = False
        self.numDisconnectedReset = False
    
    def configuratorHandler(self, mode, state=None):
        self.confthread.progress.connect(self.enableAuthButtons)
        if mode == 0:
            self.dialog.ui.buttonDownloadConf.setEnabled(False)
            self.confthread.setMode(mode)
            self.confthread.start()
        elif mode == 1:
            if state:
                self.dialog.ui.buttonDownloadConf.setEnabled(False)
                self.dialog.ui.checkAuth.setEnabled(False)
                self.confthread.setMode(mode)
                self.confthread.start()
            else:
                pass
                # Remove the files from resource folder
                # Redownload them back

    def enableAuthButtons(self):
        self.dialog.ui.buttonDownloadConf.setEnabled(True)
        self.dialog.ui.checkAuth.setEnabled(True)
        
    def startSpeedtest(self):
        #self.speedtester.start()
        #self.sendCustomMessage("Attempting to kill openvpn\n")
        self.notifySend("Disconnecting...", False)
        subprocess.call(['sudo', '%skills.sh' % self.guifolder])

    def showSpeedtestResult(self, result, updown):
        self.sendCustomMessage(updown + " speed: " + result + " Kbps\n")

    def multishot(self):
        self.turnConnectionTesterOnOff()
        self.altRefreshInfo()

    def turnConnectionTesterOnOff(self):
        self.connectionTester.start()

    def offlineDetect(self, connection):
        if not connection:
            self.activeConnection = False
            self.ui.progressBar.setFormat("OFFLINE")
            offlinestyle = "QProgressBar::chunk {background-color: #ed5050;border-radius: 4px;border: 1px solid grey;}"
            self.ui.progressBar.setStyleSheet(offlinestyle)
            self.ui.progressBar.setValue(100)
            self.ui.progressBar.setAlignment(QtCore.Qt.AlignCenter)
        elif connection:
            self.activeConnection = True
        # elif connection and self.progressBar.text() != "ONLINE":
        #     self.activeConnection = True
        #     self.progressBar.setFormat("ONLINE")
        #     onlinestyle = "QProgressBar::chunk {background-color: #b1ffa0;border-radius: 4px;border: 1px solid grey;}"
        #     self.progressBar.setStyleSheet(onlinestyle)
        #     self.progressBar.setValue(100)
        #     self.progressBar.setAlignment(QtCore.Qt.AlignCenter)
    
    def secureDetect(self, secured):
        msgonce = False
        if self.activeConnection:
            if secured:
                self.ui.progressBar.setFormat("ONLINE & SECURED")
                onlinestyle = "QProgressBar::chunk {background-color: #b1ffa0;border-radius: 4px;border: 1px solid grey;}"
                self.notifySend("Connected")
            elif not secured:
                self.ui.progressBar.setFormat("ONLINE & UNSECURED")
                onlinestyle = "QProgressBar::chunk {background-color: #c8e6c9;border-radius: 4px;border: 1px solid red;}"
                self.notifySend("Disconnected")
            self.ui.progressBar.setStyleSheet(onlinestyle)
            self.ui.progressBar.setValue(100)
            self.ui.progressBar.setAlignment(QtCore.Qt.AlignCenter)

    def add2Kill(self):
        if self.dialog.ui.lineEditKill.text() != "":
            self.dialog.ui.listKill.addItem(self.dialog.ui.lineEditKill.text())

    def removeSelectedKill(self):
        listItems = self.dialog.ui.listKill.selectedItems()
        if not listItems: 
            return
        for item in listItems:
            self.dialog.ui.listKill.takeItem(self.dialog.ui.listKill.row(item))

    def setSettings2dialog(self):
        self.settings = QtCore.QSettings()
        self.settings.beginGroup("dialog/lines")
        ## Set the default value for confpath IF empty
        if self.settings.value("confpath", type=str) == "":
            self.settings.setValue("confpath", self.guifolder + "ovpn_confs/")
        self.dialog.ui.lineEditUsername.setText(self.settings.value("username", type=str))
        self.dialog.ui.lineEditPassword.setText(self.settings.value("password", type=str))
        self.dialog.ui.lineEditDistance.setText(self.settings.value("maxdistance", type=str))
        self.dialog.ui.lineEditConfPath.setText(self.settings.value("confpath", type=str))
        if not os.path.isdir(self.settings.value("confpath", type=str)):
                os.makedirs(self.settings.value("confpath", type=str))
        self.settings.endGroup()
        self.settings.beginGroup("dialog/checkboxes")
        self.dialog.ui.checkTor.setChecked(self.settings.value("Oni", type=bool))
        self.dialog.ui.checkTV.setChecked(self.settings.value("BBC", type=bool))
        self.dialog.ui.checkP2P.setChecked(self.settings.value("P2P", type=bool))
        self.dialog.ui.checkDouble.setChecked(self.settings.value("Dou", type=bool))
        self.dialog.ui.checkAntiddos.setChecked(self.settings.value("DDo", type=bool))
        self.dialog.ui.checkStandard.setChecked(self.settings.value("Sta", type=bool))
        self.dialog.ui.checkDedicated.setChecked(self.settings.value("Ded", type=bool))
        self.dialog.ui.checkSha.setChecked(self.settings.value("sha", type=bool))
        self.settings.endGroup()

    def setDialog2Main(self):
        self.dialogCheckstates = self.getDialogCheckboxStates()
        self.dialogLineEdits = self.getDialogLineEdits()
        self.dialogKillList = self.getDialogKillList()
        self.save2settings()
        self.refreshServers(self.dialogCheckstates, self.dialogLineEdits[2][1])
    
    def save2settings(self):
        self.settings = QtCore.QSettings()
        for idx in range(len(self.dialogCheckstates)):
            self.settings.setValue("dialog/checkboxes/%s" % self.dialogCheckstates[idx][0][0:3], self.dialogCheckstates[idx][1])
        for idx in range(len(self.dialogLineEdits)):
            self.settings.setValue("dialog/lines/%s" % self.dialogLineEdits[idx][0], self.dialogLineEdits[idx][1])

    def getDialogKillList(self):
        pass

    def getDialogLineEdits(self):
        dialogLineEdits = []
        dialogLineEdits.append(["username", self.dialog.ui.lineEditUsername.text()])
        dialogLineEdits.append(["password", self.dialog.ui.lineEditPassword.text()])
        dialogLineEdits.append(["maxdistance", self.dialog.ui.lineEditDistance.text()])
        dialogLineEdits.append(["confpath", self.dialog.ui.lineEditConfPath.text()])
        return dialogLineEdits

    def getDialogCheckboxStates(self):
        dialogCheckstates = []
        dialogCheckstates.append(["Onion", self.dialog.ui.checkTor.isChecked()])
        dialogCheckstates.append(["BBC Netflix Areena Katsomo Rai ABC Hulu AXN iPlayer iTV Bandeirantes Canal Nippon TV3 Zattoo", self.dialog.ui.checkTV.isChecked()])
        dialogCheckstates.append(["P2P", self.dialog.ui.checkP2P.isChecked()])
        dialogCheckstates.append(["Double", self.dialog.ui.checkDouble.isChecked()])
        dialogCheckstates.append(["DDoS", self.dialog.ui.checkAntiddos.isChecked()])
        dialogCheckstates.append(["Standard", self.dialog.ui.checkStandard.isChecked()])
        dialogCheckstates.append(["Dedicated", self.dialog.ui.checkDedicated.isChecked()])
        dialogCheckstates.append(["sha512", self.dialog.ui.checkSha.isChecked()])
        return dialogCheckstates

    def sendCustomMessage(self, text):
        self.tester.communicator.setMessage(text)
        self.tester.communicator.start()
        self.cursor2.movePosition(self.cursor2.End)

    def textTest(self, text):
        self.cursor2 = self.ui.textareaLog.textCursor()
        self.cursor2.movePosition(self.cursor2.End)
        self.cursor2.insertText(text)
        self.ui.textareaLog.ensureCursorVisible()
    
    def altRefreshInfo(self):
        self.ui.refreshInfobtn.setEnabled(False)
        self.updater.start()

    def nordhandler(self, testnord, secured):
        self.addTableInfo(1, [testnord.ipAddress, testnord.country, testnord.town, time.strftime("%X")])
        self.secureDetect(secured)
        self.ui.refreshInfobtn.setEnabled(True)

    def refreshServers(self, checkboxes = [], distance = ""):
        self.ui.tableServers.setSortingEnabled(False)
        self.tester.setMode(2, False, checkboxes, distance)
        self.tester.start()
        self.tester.wait()
        self.ui.tableServers.setRowCount(len(self.tester.nord.dataMode))
        self.tester.setMode(1, False)
        self.tester.start()
        self.tester.wait()
        self.ui.tableServers.setSortingEnabled(True)
    
    def testProgress(self, val):
        self.ui.progressBar.setValue(val)

    # def testFunction2(self):
    #     #print(self.nord.dataServer)
    #     #print(self.nord.dataAddress[self.tableServers.currentRow()])
    #     #print(self.tableServers.currentRow())
    #     #print(self.tableServers.item(self.tableServers.currentRow(),0).text())
    #     #self.threadTest = ThreadClass(self, self.tester.nord.dataAddress[self.tester.nord.dataServer.index(self.tableServers.item(self.tableServers.currentRow(),0).text())])
    #     #self.threadTest.start()
    #     self.tableServers.setSortingEnabled(False)

    def connect_vpn(self):
        serverAddress = self.settings.value("dialog/lines/confpath", type=str) + "/ovpn_udp/%s.udp.ovpn" % self.tester.nord.dataAddress[self.tester.nord.dataServer.index(self.ui.tableServers.item(self.ui.tableServers.currentRow(), 0).text())]
        updateResolv = "/etc/openvpn/update-resolv-conf"
        cmd = "sudo openvpn --config %s --script-security 2 --up %s --down %s --daemon" % (serverAddress, updateResolv, updateResolv)
        print(cmd)
        # TODO: Investigate the pros and cons of both these methods and why the /bin/sh/ -c would be better since
        #       it breaks the communication between application console area
        # TODO: Also try to come up with a better way to ask sudo password than in a console in the background...
        self.process.start('sudo', ["openvpn", "--config", serverAddress, "--script-security", "2", "--up", updateResolv, "--down", updateResolv])
        #self.process.start('/bin/sh', ['-c', cmd])
        self.notifySend("Connecting...", False)

    def dataReady(self):
        cursor = self.ui.textareaLog.textCursor()
        cursor.insertText(str(self.process.readAll(), encoding='ascii'))
        cursor.movePosition(cursor.End)
        self.ui.textareaLog.ensureCursorVisible()

    def getTableSelectedRow(self):
        indexes = self.ui.tableServers.selectionModel().selectedRows()
        for index in sorted(indexes):
            return index.row()

    def addTableInfo(self, row, array):
        index = 0
        while index < len(array):
            if isinstance(array[index], list):
                item2 = ', '.join(array[index])
                item = QtWidgets.QTableWidgetItem()
                item.setData(QtCore.Qt.EditRole, item2)
            else:
                item = str(array[index])
                item = QtWidgets.QTableWidgetItem(item)
            item.setTextAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
            self.ui.tableInfo.setItem(row - 1, index, item)
            index += 1

    def addTableServers(self, column, array):
        #self.tableServers.setRowCount(len(array))
        index = 0
        while index < len(array):
            if isinstance(array[index], float):
                item2 = int(array[index])
                #item = str(item)
                item = QtWidgets.QTableWidgetItem()
                item.setData(QtCore.Qt.EditRole, item2)
            else:
                item = str(array[index])
                item = QtWidgets.QTableWidgetItem(item)
            self.ui.tableServers.setItem(index, column, item)
            index += 1

    def notifySend(self, msg, tracker=True):
        send = True
        if tracker:
            if msg == "Connected":
                if self.numConnected != 0:
                    send = False
                self.numConnected += 1
                self.numDisconnectedReset = True
                self.numConnectedReset = False
            elif msg == "Disconnected":
                if self.numDisconnected != 0:
                    send = False
                self.numDisconnected += 1
                self.numConnectedReset = True
                self.numDisconnectedReset = False
            if send:
                os.system('notify-send %s VPN' % msg)
        else:
            os.system('notify-send %s VPN' % msg)

    def windowSettings(self):
        self.dialog.exec()
        self.dialog.activateWindow()

    def closeEvent(self, event):
        print("LOL")
        QtWidgets.QApplication.quit()

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    print(QtWidgets.QApplication.style().metaObject().className())
    print(QtWidgets.QStyleFactory.keys())
    MainWindow = QtWidgets.QMainWindow()
    prog = PingNordVPN(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
